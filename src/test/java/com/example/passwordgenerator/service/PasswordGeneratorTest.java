package com.example.passwordgenerator.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PasswordGeneratorTest {

    private PasswordGenerator passwordGenerator;

    @BeforeEach
    public void setUp() {
        passwordGenerator = new PasswordGenerator();
    }

    @Test
    @DisplayName("Given password length eight when generate then return password")
    public void givenPasswordLengthEight_whenGenerate_thenReturnPassword() {
        String password = passwordGenerator.generate(8);

        assertEquals(8, password.length(), "Password length should be 6");
    }

    @Test
    @DisplayName("Given password length ten when generate then return password including lowercase alphabet")
    public void givenLengthTen_whenGenerate_thenReturnPasswordIncludingLowercaseAlphabet() {
        String regex = "[a-z]";

        String password = passwordGenerator.generate(10);
        int result = findMatch(regex, password);

        assertEquals(2, result, "Password should contain two uppercase alphabets");
    }

    @Test
    @DisplayName("Given password length ten when generate then return password containing two uppercase alphabet")
    public void givenLengthTen_whenGenerate_thenReturnPasswordContainingTwoUppercase() {
        String regex = "[A-Z]";

        String password = passwordGenerator.generate(10);
        int result = findMatch(regex, password);

        assertEquals(2, result, "Password should contain two uppercase alphabets");
    }

    @Test
    @DisplayName("Given password length ten when generate then return password containing four number")
    public void givenLengthTen_whenGenerate_thenReturnPasswordContainingFourNumber() {
        String regex = "[0-9]";

        String password = passwordGenerator.generate(10);
        int result = findMatch(regex, password);

        assertEquals(4, result, "Password should contain four number");
    }

    @Test
    @DisplayName("Given password length ten when generate then return password containing two symbol")
    public void givenLengthTen_whenGenerate_thenReturnPasswordContainingTwoSymbol() {
        String regex = "[_$#%]";

        String password = passwordGenerator.generate(10);
        int result = findMatch(regex, password);

        assertEquals(2, result, "Password should contain two symbol");
    }

    private int findMatch(String regex, String password) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);
        int result = 0;
        while (matcher.find()) {
            result++;
        }
        return result;
    }

}