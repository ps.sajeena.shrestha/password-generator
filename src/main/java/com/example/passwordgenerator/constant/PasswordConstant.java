package com.example.passwordgenerator.constant;

import lombok.Getter;

@Getter
public enum PasswordConstant {

    DIGIT(4, "0123456789"),
    UPPERCASE(2, "ABCDEFGHIJKLMNOPQRSTUVWXYZ"),
    LOWERCASE(0, "abcdefghijklmnopqrstuvwxyz"),
    SYMBOL(2, "_$#%");

    private final Integer number;
    private final String characterSet;

    PasswordConstant(Integer number, String characterSet) {
        this.number = number;
        this.characterSet = characterSet;
    }
}
