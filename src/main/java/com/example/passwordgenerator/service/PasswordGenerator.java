package com.example.passwordgenerator.service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static com.example.passwordgenerator.constant.PasswordConstant.*;

public class PasswordGenerator {

    public PasswordGenerator() {
    }

    public String generate(Integer length) {
        String digits = generateRandomDigits();
        String uppercase = generateRandomUppercaseAlphabets();
        String symbols = generateRandomSymbols();

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(digits)
                .append(uppercase)
                .append(symbols);

        if (stringBuilder.length() < length) {
            String lowercase = generateRandomLowercaseAlphabets(length - stringBuilder.length());
            stringBuilder.append(lowercase);
        }
        return shuffleString(stringBuilder.toString());
    }

    private String generateRandomDigits() {
        return generateRandomWords(DIGIT.getNumber(), DIGIT.getCharacterSet());
    }

    private String generateRandomUppercaseAlphabets() {
        return generateRandomWords(UPPERCASE.getNumber(), UPPERCASE.getCharacterSet());
    }

    private String generateRandomLowercaseAlphabets(Integer number) {
        return generateRandomWords(number, LOWERCASE.getCharacterSet());
    }

    private String generateRandomSymbols() {
        return generateRandomWords(SYMBOL.getNumber(), SYMBOL.getCharacterSet());
    }

    private String generateRandomWords(Integer number, String characterSet) {
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        while (stringBuilder.length() < number) {
            int index = (int) (random.nextFloat() * characterSet.length());
            stringBuilder.append(characterSet.charAt(index));
        }
        return stringBuilder.toString();
    }

    public static String shuffleString(String input) {
        List<String> result = Arrays.asList(input.split(""));
        Collections.shuffle(result);
        return String.join("", result);
    }
    
}
