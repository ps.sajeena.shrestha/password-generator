package com.example.passwordgenerator;

import com.example.passwordgenerator.service.PasswordGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PasswordGeneratorApplication {

    public static void main(String[] args) {
        PasswordGenerator passwordGenerator = new PasswordGenerator();
        System.out.println("password : " + passwordGenerator.generate(10));
        SpringApplication.run(PasswordGeneratorApplication.class, args);
    }

}
